using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace WebApiJwt
{
    public class HelloWorldMiddleware
    {
        private readonly RequestDelegate _next;
        public HelloWorldMiddleware(RequestDelegate next)
        {
            this._next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            await context.Response.WriteAsync("HelloWorldMiddleware (use in Class)\n");
            await this._next(context);
        }
    }
}