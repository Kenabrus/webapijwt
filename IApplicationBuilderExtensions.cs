using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace WebApiJwt
{
    public static class IApplicationbuilderExtension
    {
        public static void RunHelloWorld(this IApplicationBuilder app)
        {
            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("response from extension app.Run()");
            }
            );
        }

        public static IApplicationBuilder UseHelloWorld(this IApplicationBuilder app)
        {
            return app.Use(async (context, next) =>
            {
                await context.Response.WriteAsync("UseHelloWorld return app.Use()");
                await next();
            });
        }

        // public static IApplicationBuilder UseHelloWorldInClass(this IApplicationBuilder app)
        // {
            
        // }
    }
}
